var express = require('express'),
  _ = require('underscore'),
  authData = require('../../constants/auth'),
  request = require('request'),
  querystring = require('querystring');



var updateRouter = function(router) {
  router.route('/ok')
    .get(function(req, res) {
      var url = 'https:' + authData.ok.baseUrl + '?' + querystring.stringify({
        code: req.query.code,
        client_id: authData.ok.appId,
        client_secret: authData.ok.secret,
        redirect_uri: 'https://nikita.accshop.org/auth/ok',
        grant_type: 'authorization_code'
      });
      console.log(url);
      res.json({ da: 'da' });
      // request.post(url, function (error, response, body) {
      //   if (!error && response.statusCode == 200) {
      //     console.log(body) // Show the HTML for the Google homepage. 
      //   }
      // })


    })
  return router;


}

module.exports = updateRouter;
