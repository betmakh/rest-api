var express = require('express'),
  _ = require('underscore'),
  authData = require('../../constants/auth'),
  request = require('request'),
  querystring = require('querystring'),
  userUtil = require('../../auth/userUtil'),
  User = require('../../db/models/userModel'),
  // loadTemplates = require('../tmp/loadTemplates')
  basicLayout = require('../tmp/basicLayout');





var updateRouter = function(router) {
  router.route('/resetPassword')
    .get((req, res) => {
      var failed = function(err) {
          res.status(500).send(err);
      }
      var token = req.query.token;
      userUtil.decodeData(token).then(data => {
        if (data.action == 'passwordReset') {
          userUtil.getUser(data, (err, person) => {
            basicLayout('public/rest-client/login/templates/resetPasswordConfirmation.hbs', person.toJSON()).then(data => {
              res.render('mainPageLayout', data);
            }, failed)
          })
        } else {
          failed('wrong action')
        }
      });

    })
  router.route('/registrationConfirm')
    .get(function(req, res) {
      var token = req.query.token;
      userUtil.decodeData(token).then(data => {
        console.log(data);
        User.findOne({
          'email': data.email
        }, 'is_email_confirmed name email', (err, person) => {
          person.is_email_confirmed = true;
          person.save((err) => {
            if (!err) {
              basicLayout('public/rest-client/login/templates/confirmEmail.hbs', person.toJSON()).then(data => {
                res.render('mainPageLayout', data);
              })
            } else {
              res.status(500).render('mainPageLayout', err);
            }
          })
        })
      });

    });

  return router;

}

module.exports = updateRouter;
