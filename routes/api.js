var express = require('express');
var router = express.Router();
var _ = require('underscore');
var routers = [require('./api/users'),
  require('./api/auth'),
  require('./api/account'),
  require('./api/pay'),
  require('./api/feedbacks')
];


router.use(function(req, res, next) {
  // do logging
  console.log('Requested api');
  console.log(req.originalUrl);
  next(); // make sure we go to the next routes and don't stop here
});
/* GET home page. */
router.get('/', function(req, res) {
  res.json({
    message: 'hooray! welcome to our api!'
  });
});

routers.forEach(function(ruoterUpdate) {
  router = ruoterUpdate(router);
})

// router = _.extend(router, usersRoute);
// router = _.extend(router, authRoute);
// router = _.extend(router, accRoute);

module.exports = router;
