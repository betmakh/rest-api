var express = require('express');
var router = express.Router();
var _ = require('underscore');
var routers = [require('./auth/ok'),require('./auth/registration')];


router.use(function(req, res, next) {
  // do logging
  console.log('Requested auth');
  console.log(req.originalUrl);
  next(); // make sure we go to the next routes and don't stop here
});
/* GET home page. */
router.get('/', function(req, res) {
  res.json({
    message: 'hooray! welcome to our api!'
  });
});

routers.forEach(function(ruoterUpdate) {
  router = ruoterUpdate(router);
})

// router = _.extend(router, usersRoute);
// router = _.extend(router, authRoute);
// router = _.extend(router, accRoute);

module.exports = router;
