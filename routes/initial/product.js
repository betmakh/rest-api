var Account = require('../../db/models/accountModel'),
	loadTemplates = require('../tmp/loadTemplates');

module.exports = function(router) {

  router.route('/product/:id')
    .get(function(req, res) {
      console.log(req);
      Account.findById(req.params.id).populate('user').exec(function(err, acc) {
        var templatesPaths = [
          'public/rest-client/layout/templates/header.hbs',
          'public/rest-client/layout/templates/banner.hbs',
          'public/rest-client/layout/templates/footer.hbs',
          'public/rest-client/product/templates/product.hbs',
        ];

        loadTemplates(templatesPaths, function(templates) {
          res.render('mainPageLayout', {
            header: templates.header({}),
            footer: templates.footer({}),
            items: templates.product(acc.toJSON())
          })
        })
        // res.render('mainPageLayout')
      });
    })

  return router;
}
