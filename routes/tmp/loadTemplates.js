var fs = require('fs'),
	path = require('path'),
	Handlebars = require('handlebars');

module.exports = function(paths) {
	var templates = {};
	return new Promise( (resolve, reject) => {
		paths.forEach(function(src) {
			console.log('src', src);
			fs.readFile(src,'utf-8', function(err,data) {
				console.log('err', err);
				if (err) {
					reject(err);
				} else {
					templates[path.basename(src,'.hbs')] = Handlebars.compile(data);
				}
				if (Object.keys(templates).length == paths.length) {
					resolve(templates);
				}
			})
		})
	})
}