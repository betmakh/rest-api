var fs = require('fs'),
    path = require('path'),
    Handlebars = require('handlebars'),
    loadTemplates = require('./loadTemplates');

// prepare templates for render
module.exports = function(mainTemplateSrc, data) {
    var templatesPaths = [
        'public/rest-client/layout/templates/header.hbs',
        'public/rest-client/layout/templates/footer.hbs'
    ];
    templatesPaths.push(mainTemplateSrc)
    console.log('basic layout')
    return new Promise((resolve, reject) => {
        loadTemplates(templatesPaths).then(templates => {
            resolve({
                header: templates.header({}),
                footer: templates.footer({}),
                items: templates[path.basename(mainTemplateSrc,'.hbs')](data || {})
            });
        }, reject)
    })
}