var express = require('express');
var router = express.Router();
var Handlebars = require('handlebars');
var fs = require('fs');
var path = require('path');
var loadTemplates = require('./tmp/loadTemplates');
var routes = [
	require('./initial/product')
];

/* GET home page. */

routes.forEach(function(updateRouter){
		router = updateRouter(router);
});

router.get('/', function(req, res, next) {
	var templatesPaths = [
		'public/rest-client/layout/templates/header.hbs',
		'public/rest-client/layout/templates/banner.hbs',
		'public/rest-client/layout/templates/footer.hbs'
	];

	loadTemplates(templatesPaths).then(function(templates){
		res.render('mainPageLayout', {
			header: templates.header({}),
			banner: templates.banner({}),
			footer: templates.footer({})
		});
	})


});



module.exports = router;
