var express = require('express');
// var router = express.Router();
var User = require('../../db/models/userModel');
var Account = require('../../db/models/accountModel');
var jwt = require('jsonwebtoken');
var googleAuth = require('../../auth/google');
var _ = require('underscore'),
    userUtil = require('../../auth/userUtil'),
    crypto = require('crypto');

var app = require('../../app.js');



var updateRouter = function(router) {
    // router.route('/users')
    //   .post(function(req, res) {

    //     var user = new User(); // create a new instance of the user model
    //     user.avatar = req.body.avatar;
    //     user.name = req.body.name; // set the users name (comes from the request)
    //     user.save(function(err) {
    //       if (err) {
    //         res.send(err);
    //       }

    //       res.json({
    //         message: 'User created!'
    //       });
    //     });

    //   })
    //   .get(function(req, res) {
    //     User.find(function(err, users) {
    //       if (err) {
    //         res.send(err);
    //       }
    //       res.json(users);
    //     });
    //   });

    router.route('/users/isEmailAvailable')
        .get(function(req, res) {
            var email = req.query.email;
            User.findOne({
                'email': email
            }, function(err, person) {
                if (!person) {
                    res.json({
                        isFree: true
                    })
                } else {
                    res.json({
                        isFree: false
                    })
                }
            });
        });
    router.route('/users/resetPasswordConfirm')
        .post((req, res) => {
            var failed = err => {
                res.status(500).send(err);
            }
            userUtil.detectUser(req.body.token, (err, person) => {
              if (err) failed(err);
              person.password = crypto.createHash('md5').update(req.body.password).digest("hex");
              person.save().then(() => {
                res.sendStatus(200);
              }, failed)
            });
        })

    router.route('/users/resetPassword')
        .post(function(req, res) {
            var email = req.body.email;
            User.findOne({
                'email': email
            }, (err, person) => {
                if (!person) {
                    res.send(err)
                } else {
                    userUtil.encodeUser({
                        user: person,
                        fields: ['email', '_id'],
                        data: {
                            action: 'passwordReset'
                        }
                    }, (err, token) => {
                      
                        console.log('sending reset email to');
                        console.log('email', email);
                        console.log('person.email', person.email);
                        app.mailer.send('mail/resetPassword', {
                            to: person.email, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
                            subject: 'Reset AccShop user password', // REQUIRED.
                            userName: person.name, // All additional properties are also passed to the template as local variables.
                            link: req.protocol + '://' + req.get('host') + '/auth/resetPassword?token=' + token // All additional properties are also passed to the template as local variables.
                        }, function(err) {
                            if (err) {
                                // handle error
                                res.status = 500;
                                res.send('There was an error sending the email');
                                return;
                            }
                            res.json({
                                    name: person.name
                                })
                                // res.send('Email Sent');
                        });
                    })
                }
            });
        });

    router.route('/users')
        .post(function(req, res) {
            var user = {};
            var dbUser = new User();
            console.log(req.body);
            dbUser.name = req.body.name,
            dbUser.password = crypto.createHash('md5').update(req.body.password).digest("hex");
            dbUser.email = req.body.email;
            dbUser.save(function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    console.log(dbUser);

                    userUtil.encodeUser({
                            user: dbUser,
                            fields: ['email', '_id']
                        }, function(err, token) {
                            console.log('encoded');
                            if (err) {
                                res.status(500).send(err);
                                return;
                            }
                            app.mailer.send('mail/registerMail', {
                                to: dbUser.email, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
                                subject: 'New user registation', // REQUIRED.
                                userName: dbUser.name, // All additional properties are also passed to the template as local variables.
                                link: req.protocol + '://' + req.get('host') + '/auth/registrationConfirm?token=' + token
                            }, function(err) {
                                if (err) {
                                    res.status(500).send(err);
                                    return;
                                }
                                res.json({
                                    token: token,
                                    name: dbUser.name
                                })
                            });

                        })
                        // res.json(dbUser);
                }
            })

        })

    router.route('/users/:id')
        .get(function(req, res) {
            console.log(req.params.id);
            if (req.params.id == '0') {
                var token = req.headers.authorization;
                var callbackForResponse = function(err, person) {
                    console.log(person);
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        User.findById(person._id).populate('accounts').populate('liked').populate('ordersBuy').populate('ordersSell').exec(function(err, user) {
                            if (user) {
                                res.json(user);
                            } else {
                                res.send(err);
                            }
                        })
                    }
                }
                if (req.headers.authorizationtype == 'google') {
                    googleAuth.detectUser(token, callbackForResponse);
                } else {
                    userUtil.detectUser(token, callbackForResponse)
                }
            } else {
                User.findById(req.params.id).populate('accounts').populate('liked').populate('ordersBuy').populate('ordersSell').exec(function(err, user) {
                    console.log(arguments);
                    if (err) {
                        res.send(err)
                    } else {
                        res.json(user);
                    }
                }, function(err) {
                    res.json(err)
                });
            }

        })

    .put(function(req, res) {

        // use our bear model to find the bear we want
        User.findById(req.params.id, function(err, user) {

            if (err) {
                res.send(err);
            }
            _.extend(user, req.body);
            // user.name = req.body.name; // update the bears info
            // save the bear
            user.save(function(err) {
                if (err) {
                    res.send(err);
                }

                res.json({
                    message: 'User updated!'
                });
            });

        });
    });

    return router;
}



module.exports = updateRouter;