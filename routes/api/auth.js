var express = require('express');
var oauth2Client = require('../../auth/google');
var google = require('googleapis');
var plus = google.plus('v1');
var _ = require('underscore');
var User = require('../../db/models/userModel'),
  authData = require('../../constants/auth'),
  querystring = require('querystring'),
  jwt = require('jsonwebtoken'),
  fs = require('fs'),
  crypto = require('crypto'),
  request = require('request'),
  userUtil = require('../../auth/userUtil');


// var key = fs.readFileSync('key/id_rsa');

var updateRouter = function(router) {

  
  router.route('/auth/user')
    .post((req, res) => {
      var data = req.body;
      User.findOne({
        'email' : data.email
      }, 'name password avatar', (err, person) => {
        if (!person) {
          res.status(401);
          res.json({
            message: 'password or email is incorrect'
          });
        } else {
          var md5pass = crypto.createHash('md5').update(data.password).digest("hex");
          if (person.password === md5pass) {
            userUtil.encodeUser({user: person}, (err, token) => {
              if (err) {
                res.status(401)
                res.json({
                  message: 'password or email is incorrect'
                })
              } else {
                res.json({
                  token: token,
                  name: person.name,
                  avatar: person.avatar
                });
              }
            })
          } else {
            res.status(401)
            res.json({
              message: 'password or email is incorrect'
            })
          }
        }
      })
    })
  router.route('/auth/google')
    .post(function(req, res) {
      var code = req.body.code;
      console.log(oauth2Client);
      oauth2Client.getToken(code, function(err, tokens) {
        // Now tokens contains an access_token and an optional refresh_token. Save them.
        if (!err) {
          var user = {};
          oauth2Client.setCredentials(tokens);
          plus.people.get({
            userId: 'me',
            auth: oauth2Client
          }, function(error, response) {
            if (!error) {
              user.name = response.displayName;
              user.email = (_.find(response.emails, function(el) {
                return el.type == 'account'
              })).value;
              user.avatar = response.image.url;
              User.findOne({
                  'email': user.email
                }, function(err, person) {
                  console.log(person);
                  if (!person) {
                    var dbUser = new User();
                    dbUser = _.extend(dbUser, user);
                    dbUser.save(function(err) {
                      if (err) {
                        res.send(err);
                      }
                      res.json(dbUser);
                    })
                  } else {
                    res.json(person);
                  }
                })
                // res.json(user)
            } else {
              console.log(error);
            }
          });
          console.log(tokens);
        } else {
          console.log('code not ferified');
          res.json(err)
        }
      });


    })

  router.route('/auth/ok')
    .post(function(req, res) {
      var code = req.body.code;
      var url = 'https:' + authData.ok.baseUrl + '?' + querystring.stringify({
        code: code,
        client_id: authData.ok.appId,
        client_secret: authData.ok.secret,
        redirect_uri: 'https://nikita.accshop.org/auth/ok',
        grant_type: 'authorization_code'
      });
      console.log(url);
      var makeMD5 = function(access_token) {
        var str = crypto.createHash('md5').update(access_token + authData.ok.secret).digest("hex");
        // var toDo = 
        str = 'application_key=' + authData.ok.public + 'method=users.getCurrentUser' + str;
        return crypto.createHash('md5').update(str).digest("hex");
      }

      var getUserInfo = function(access_token, cb) {
        console.log(access_token);
        var url = 'https://api.ok.ru/fb.do?' + querystring.stringify({
          application_key: authData.ok.public,
          method: 'users.getCurrentUser',
          access_token: access_token,
          sig: makeMD5(access_token)
        });
        console.log(url);
        request(url, function(error, response, body) {
          cb(body);
        });
      }

      request.post(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log(body) // Show the HTML for the Google homepage. 
          getUserInfo(JSON.parse(body).access_token, function(data) {
            var okUser = JSON.parse(data);
            var token = jwt.sign({
              email: okUser.email
            }, fs.readFileSync('key/id_rsa'));
            var userData = {
              name: okUser.name,
              avatar: okUser.pic_3,
              email: okUser.email
            }
            User.findOne({
              'email': okUser.email
            }, function(err, person) {
              if (!person) {
                var dbUser = new User();
                dbUser = _.extend(dbUser, userData);
                dbUser.save(function(err) {
                  if (err) {
                    res.send(err);
                  } else {
                    delete userData.email;
                    userData.token = token;
                    res.json(userData);
                  }
                })
              } else {
                delete userData.email;
                userData.token = token;
                userData.name = person.name;
                userData.avatar = person.avatar;
                res.json(userData);
              }
            });
            // res.json(JSON.parse(data));
          })
        }
      })
    })

  router.route('/auth/vk')
    .post(function(req, res) {
      var code = req.body.code;
      var url = 'https:' + authData.vk.baseUrl + '?' + querystring.stringify({
        code: code,
        scope: 'email',
        client_id: authData.vk.appId,
        client_secret: authData.vk.secret,
        redirect_uri: 'https://nikita.accshop.org/auth/ok',
      });
      console.log(url);

      var getVKUser = function(accessToken, cb) {
          console.log('getVKUser');
        var url = 'https:' + authData.vk.apiUrl + '/users.get?' + querystring.stringify({
          access_token: accessToken,
          fields: 'photo_200_orig'
        });

        request(url, function(error, response, body) {
          cb(error, response, body)
        })
      }

      request(url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          var data = JSON.parse(body);
          var token = jwt.sign({
            email: data.email
          }, fs.readFileSync('key/id_rsa'));

          User.findOne({
            'email': data.email
          }, function(err, person) {
            if (!person) {
              getVKUser(data.access_token, function(error, response, body) {
                if (error) {
                  res.json(error)
                } else {
                  body = JSON.parse(body)
                  var userData = {
                    email: data.email,
                    name: body.response[0].first_name + ' ' + body.response[0].last_name,
                    avatar: body.response[0].photo_200_orig,
                  }
                  console.log(body)
                  var dbUser = new User();
                  dbUser = _.extend(dbUser, userData);
                  dbUser.save(function(err) {
                    if (err) {
                      res.send(err);
                    } else {
                      delete userData.email;
                      userData.token = token;
                      res.json(userData);
                    }
                  })

                }
              })
            } else {
              var userData = {};
              userData.token = token;
              userData.name = person.name;
              userData.avatar = person.avatar;
              res.json(userData);
            }
          });
          //   // res.json(JSON.parse(data));
          // })
        }
      })
    })
  return router;


}

module.exports = updateRouter;
