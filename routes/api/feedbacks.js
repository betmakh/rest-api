var express = require('express');
var oauth2Client = require('../../auth/google');
var google = require('googleapis');
var plus = google.plus('v1');
var _ = require('underscore');
var User = require('../../db/models/userModel');
var googleAuth = require('../../auth/google');
var Feedback = require('../../db/models/feedbackModel'),
  detectUser = require('../../auth/userUtil').detectUser;



var updateRouter = function(router) {
  router.route('/feedbacks')
    .get(function(req, res) {
      var pageOptions = {
        page: Number(req.query.page) || 0,
        limit: Number(req.query.limit) || 10
      }
      var calculateTotal = Feedback.count().exec();
      Feedback.find()
        .skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit)
        .populate('user')
        .exec(function(err, feedbacks) {
          if (err) {
            res.sendStatus(500).json(err);
          } else {
            calculateTotal.then((total) => {
                var data = {
                  options: {
                    totalItems: total,
                    page: pageOptions.page,
                    limit: pageOptions.limit,
                    totalPages: Math.ceil(total / pageOptions.limit)
                  },
                  data: feedbacks
                }
                res.json(data);
            })
          }
        })

    })
    .post(function(req, res) {
      var token = req.headers.authorization,
        type = req.headers.authorizationtype;

      var callbackForResponse = function(err, person) {
        if (err) {
          res.statusCode = 403;
          res.json({
            status: res.statusCode,
            message: 'try to relogin'
          });
        } else {
          var feedback = new Feedback();
          _.extend(feedback, req.body)
          feedback.user = person._id
          feedback.save().then(function() {
            res.json(_.extend(feedback, { user: person }));
          })
        }
      }
      if (type == 'google') {
        googleAuth.detectUser(token, callbackForResponse)
      } else {
        detectUser(token, callbackForResponse)
      }
    })

  router.route('/feedbacks/:id')
    .get(function(req, res) {
      Feedback.findById(req.params.id).populate('user').exec(function(err, feedback) {
        if (err) {
          res.send(err);
        } else {
          res.json(feedback);
        }
      })

    })


  return router;


}

module.exports = updateRouter;
