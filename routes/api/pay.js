var express = require('express');
var User = require('../../db/models/userModel');
var Account = require('../../db/models/accountModel');
var Order = require('../../db/models/orderModel');
var jwt = require('jsonwebtoken');
var googleAuth = require('../../auth/google');
var liqpay = require('../../pay/liqpay'),
  detectUser = require('../../auth/userUtil').detectUser;


var liqpayConst = {
  private_key: '1we4iURDkyNtQ9j81qoHjoA8fO63JOyOAj1VK0Cx',
  public_key: 'i62824188565'
};

var liqPayInstance = new liqpay(liqpayConst.public_key, liqpayConst.private_key);



var updateRouter = function(router) {
  router.route('/pay/confirm')
    .post(function(req, res) {
      console.log('confirm');
      console.log(req.body);
      var result = liqPayInstance.str_to_sign(liqpayConst.private_key + req.body.data + liqpayConst.private_key)
      console.log(result);
      var buf = new Buffer(req.body.data, 'base64');
      if (result == req.body.signature) {
        var data = JSON.parse(buf.toString('ascii'));
        Order.findById(data.order_id, function(err, order) {
          order.status = data.status;
          order.save(function(err) {
            if (err) {
              console.log(err);
            } else {
              console.log(order);
              res.json({
                message: 'Status updated'
              })
            }
          });
        })
        console.log('verified');
        // console.log(buf.toString('ascii'));

      } else {
        console.log(buf);

      }

    });

  router.route('/orders/:id')
    .get(function(req, res) {
      var id = req.params.id;
      Order.findById(id, function(err, order) {
        if (err) {
          res.send(err);
        } else {
          res.json(acc);
        }
      })
    })

  router.route('/orders')
    .get(function(req, res) {
      console.log(req.query);
      var sendResponse = function(err, orders) {
        if (err) {
          res.send(err);
        } else {
          orders.reverse();
          res.json(orders);
        }
      }

      if (req.query.buyer) {
        Order.find({
          'buyer_id': req.query.buyer
        }, sendResponse)
      } else if (req.query.seller) {
        Order.find({
          'seller_id': req.query.seller
        }, sendResponse)
      }
    });

  router.route('/buy/:id')
    .post(function(req, res) {
      var token = req.headers.authorization,
        type = req.headers.authorizationtype;
      var order;

      var callbackForResponse = function(err, person) {
        if (err) {
          res.json(err);
        } else {
          order = new Order();
          // _.extend(order, req.params);
          Account.findById(req.params.id, function(err, acc) {
            if (err) {
              res.send(err);
            } else {
              order.product_id = req.params.id;
              order.title = acc.title
              order.buyer_id = person._id;
              order.seller_id = req.body.seller_id;
              order.price = req.body.price;
              order.save(function(err) {
                if (err) {
                  res.send(err);
                } else {
                  console.log(order);
                  var form = `<form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
                    <input type="hidden" name="ik_co_id" value="569eb6bc3c1eaf16218b456a" />
                    <input type="hidden" name="ik_pm_no" value="${order._id}" />
                    <input type="hidden" name="ik_am" value="${order.price}" />
                    <input type="hidden" name="ik_cur" value="UAH" />
                    <input type="hidden" name="ik_desc" value="buying WOT acc: ${order.product_id}" />
                          <input type="submit" value="Pay">
                    </form>`;
                  // var form = liqPayInstance.cnb_form({
                  //   order_id: order._id,
                  //   amount: order.price,
                  //   version: 3,
                  //   currency: 'UAH',
                  //   description: 'buying WOT acc: ' + order.product_id,
                  //   sandbox: 1,
                  // });
                  res.json({
                    form: form,
                    message: "Reqdy for pay"
                  });
                }
              });
            }

          })
          console.log(person.email);

        }
      }

      if (type == 'google') {
        googleAuth.detectUser(token, callbackForResponse)
      } else {
        detectUser(token, callbackForResponse)
      }



      // var htmlForm = liqPayInstance.cnb_form({
      //   currency: 'USD',
      // })


    })

  return router;
}



module.exports = updateRouter;
