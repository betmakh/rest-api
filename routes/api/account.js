var express = require('express');
// var router = express.Router();
var Account = require('../../db/models/accountModel');
var User = require('../../db/models/userModel');
var jwt = require('jsonwebtoken');
var googleAuth = require('../../auth/google');
var _ = require('underscore'),
  userUtil = require('../../auth/userUtil');


var updateRouter = function(router) {
  router.route('/accounts')
    .post(function(req, res) {
      // req.body = JSON.parse(req.body);
      var token = req.headers.authorization,
        type = req.headers.authorizationtype;

      var userCallback = function(err, person) {
        if (!person) {
          res.statusCode = 403;
          res.json({
            message: 'try to relogin(person not found)'
          });
        } else {
          var acc = new Account({
            user: person._id
          });
          delete req.body.id_token;
          var parsedStats = JSON.parse(req.body.statistic);
          delete req.body.statistic;
          acc = _.extend(acc, req.body);
          acc.tanks = req.body.tanks.split(',');
          acc.statistic = parsedStats;
          acc.save(function(err, account) {
            if (err) {
              res.send(err);
            } else {
              res.json(account);
            }
          });

          // res.json(person);
        }
      }
      if (type == 'google') {
        googleAuth.detectUser(token, userCallback)
      } else {
        userUtil.detectUser(token, userCallback)
      }
    })
    .get(function(req, res) {
      console.log(req.query);
      var sendResponse = function(err, accs) {
        if (err) {
          res.send(err);
        } else {
          accs.reverse();
          res.json(accs);
        }
      }

      if (req.query.user) {
        Account.find({
          'user.id': req.query.user
        }, sendResponse)
      } else {
        console.log('find accs')
        Account.find().populate('user').exec(sendResponse);
      }
    });

  router.route('/accounts/:id')
    .get(function(req, res) {
      Account.findById(req.params.id).populate('user').exec(function(err, acc) {
        if (err) {
          res.send(err);
        } else {
          res.json(acc);
        }
      });
    })
    .put(function(req, res) {
      var token = req.headers.authorization,
        accId = req.params.id;
      var callbackForResponse = function(err, person) {
        if (!person) {
          res.send(err);
        } else {
          if (person.accounts.indexOf(accId) != -1) {
            Account.findById(accId, Object.keys(req.body).join(' '), (err, acc) => {
              if (!acc) {
                res.send(err)
              } else {
                acc = _.extend(acc, req.body);
                acc.save().then(() => {
                  res.json({
                    message: 'Saved succesfull'
                  });
                }, () => {
                  res.statusCode = 403;
                  res.json({
                    message: 'try to relogin(person not found)'
                  });
                })
              }
            });

          } else {
            res.statusCode = 403;
            res.json({
              status: 403,
              message: 'This is not your accont'
            });
          }
        }
      }
      if (req.headers.authorizationtype == 'google') {
        googleAuth.detectUser(token, callbackForResponse);
      } else {
        userUtil.detectUser(token, callbackForResponse)
      }


      googleAuth.detectUser(req.headers.authorization, function(err, person) {
        Account.findById(req.params.id, function(err, acc) {
          if (req.headers.type == 'like') {
            acc.likes.push(person._id);
            console.log(_.uniq(acc.likes));
            acc.likes = _.uniq(acc.likes);
            console.log(acc.likes);
            acc.save().then(function() {
              res.json({
                message: 'Saved succesfull'
              });
            }, function() {
              res.statusCode = 403;
              res.json({
                message: 'try to relogin(person not found)'
              });
            });
          }
        });

      });

    })
    .delete(function(req, res) {
      var token = req.headers.authorization,
        type = req.headers.authorizationtype,
        accId = req.params.id;

      var callbackForResponse = function(err, person) {
        if (!person) {
          res.send(err);
        } else {
          if (person.accounts.indexOf(accId) != -1) {
            Account.remove({
              '_id': accId
            }, function(err) {
              if (err) {
                res.send(err);
              } else {
                res.json({
                  message: 'account deleted'
                });
              }
            });

          } else {
            res.statusCode = 403;
            res.json({
              message: 'This is not your accont'
            });
          }
        }
      }
      if (req.headers.authorizationtype == 'google') {
        googleAuth.detectUser(token, callbackForResponse);
      } else {
        userUtil.detectUser(token, callbackForResponse)
      }

    });

  return router;
}


module.exports = updateRouter;
