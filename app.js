var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

require('babel-register');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const ReactRouter = require('react-router-dom');
const StaticRouter = ReactRouter.StaticRouter;
const ReactApp = require('./public/react-accshop/src/App').default;

var mongoose = require('./db/connection');

var app = express();

var mailer = require('express-mailer');

mailer.extend(app, {
  from: 'support@accshop.org',
  host: 'smtp.yandex.com', // hostname
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: 'support@accshop.org',
    pass: 'anakin1488tangiers'
  }
});

module.exports = app;

// view engine setup
app.set('views', [path.join(__dirname, 'views'), path.join(__dirname, 'public/rest-client')]);
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(
  require('node-sass-middleware')({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true,
    sourceMap: true
  })
);
app.use('/public', express.static(path.join(__dirname, 'public')));

// mailer setup

var routes = require('./routes/index');
var auth = require('./routes/auth');
var api = require('./routes/api');
// app.use('/', routes);
app.use('/api', api);
// app.use('/auth', auth);
app.get('/*', (req, res) => {
  const context = {};
  const body = ReactDOMServer.renderToString(
    React.createElement(StaticRouter, { location: req.url, context }, React.createElement(ReactApp))
  );

  if (context.url) {
    res.redirect(context.url);
  }

  console.log('body', body);
  res.render('empty', { body });
  // res.sendFile(path.join(__dirname, 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
