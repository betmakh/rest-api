define([
    'underscore',
    'handlebars',
    'text!account/templates/basicInfo.hbs',
    'text!account/templates/wotProfile.hbs',
    'text!account/templates/tanks.hbs',
    'text!account/templates/tank.hbs',
], function (_, Handlebars, account, wotProfile, tanks, tank) {


    var templates = {
        account: Handlebars.compile(account),
        wotProfile: Handlebars.compile(wotProfile),
        tanks: Handlebars.compile(tanks),
        tank: Handlebars.compile(tank),
    };

    return templates;
});