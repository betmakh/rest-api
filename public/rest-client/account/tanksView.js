define([
  'marionette',
  'app',
  'account/templates/templates',
  'constants/constants',
  'entities/tanks'

], function(Marionette, app, templates, constants) {

	var TankView = Marionette.ItemView.extend({
		template: templates.tank,
		className: 'col-md-2 col-sm-3 col-xs-4'
	})

	var TanksView = Marionette.CompositeView.extend({
		template: templates.tanks,
		childViewContainer: '#tanksList',
		childView: TankView
	});

	return TanksView;

})