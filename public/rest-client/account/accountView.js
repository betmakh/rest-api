define([
  'marionette',
  'app',
  'account/templates/templates',
  'constants/constants',
  'constants/util',
  'account/tanksView',
  'entities/tanks',
  'entities/items'

], function(Marionette, app, templates, constants,util, TanksView) {
  var accModel = Backbone.Model.extend({

  })

  var AccView = Marionette.ItemView.extend({
    events: {
      'click .loginWot': 'warLogin',
      'click #proceed': 'proceed'
    },
    initialize: function () {
       this.model = app.request('account:model');
    },
    className: 'container',
    template: templates.account,
    proceed: function(e) {
      var view = this;
      e.preventDefault();
      if (this.userData && this.userTanks) {
        var objToSend = {
          title: view.$("[name='title']").val(),
          price: parseInt(view.$("[name='price']").val()),
          description: view.$("[name='description']").val(),
          tanks: (_.pluck(view.userTanks, 'tank_id')).join(','),
          is_with_email: view.$('[name="isWithEmail"]:checked').length ? true : false,
          is_bound_to_phone: view.userData.private.is_bound_to_phone,
          wot_acc_id: parseInt(view.parameters.account_id),
          acc_id: Number(view.parameters.account_id),
          statistic: JSON.stringify({
            gold: view.userData.private.gold,
            free_xp: view.userData.private.free_xp,
            win_rate: parseFloat(view.userData.statistics.all.wins / view.userData.statistics.all.battles * 100),
            battles: view.userData.statistics.all.battles
          }),
        }
        var model = this.model;

        console.log(objToSend);
        
        model.save(objToSend).then(function () {
          require(['router/appRouter'], function (router) {
           router.navigate('/product/' + model.id, {trigger: true})
          })
        });
      }
    },
    render: function() {
      this.$el.html(this.template({greeting: 'Продавай акк! СРОЧНО!'}));
      return this;
    },
    getStatistic: function() {
      var view = this;
      var userdata = app.request('userData', {
        application_id: constants.wotAppId,
        access_token: view.parameters.access_token,
        account_id: view.parameters.account_id
      });
      $.when(userdata).done(function(data) {
        view.$('#userInfo').text(data.nickname)
        view.userData = data;
      });
    },
    fetchTankCollection: function(collection) {
      var view = this;
      var tanksArr = collection.toJSON();
      var tanks = new Backbone.Collection();
      view.userTanks = tanksArr;
      view.getStatistic();
      var allTanksLoaded = function () {
        if (tanks.length === tanksArr.length) {
          var tanksView = new TanksView({
            collection: tanks
          });
          var tanksRegion = new Marionette.Region({
            el: view.$('#tanks')
          })
          tanksRegion.show(tanksView)
        }
      }
      tanksArr.forEach(function(tank) {
        var tankInfo = app.request('tank:id', {
          application_id: constants.wotAppId,
          tank_id: tank.tank_id
        });
        tankInfo.then(function (tankModel) {
          tanks.add(tankModel);
          allTanksLoaded();
        }, function (resp) {
          tanks.add(new Backbone.Model({}));
          allTanksLoaded()
        })
        // $.when(tankInfo).done(function(data) {
        //   tanks.add(data);
        //   if (tanks.length === tanksArr.length) {
        //     if (!view.tanksView) {
        //       view.tanksView = new TanksView({
        //         collection: tanks
        //       })
        //     } else {
        //       view.tanksView.collection = tanks;
        //     }
        //     var tanksRegion = new Marionette.Region({
        //       el: view.$('#tanks')
        //     })
        //     tanksRegion.show(view.tanksView)
        //       // view.tanksView.el = view.$('#tanks')[0];
        //       // view.tanksView.render();
        //       // view.$('#tanks')[0] = view.tanksView.el; 
        //   }
        // })
      })
    },
    decodeWarParams: function(url) {
      var view = this;
      this.parameters = util.parseQueryString(url)
      var tanks = app.request('userTanks:list', {
        application_id: constants.wotAppId,
        account_id: this.parameters.account_id
      });
      $.when(tanks).done(function(collection) {
        view.fetchTankCollection(collection)
        view.$('#proceed').removeClass('hidden');
        view.$('.loginWot').addClass('hidden');
        console.log(collection);
      })

      // this.tankParams = parameters;
      // console.log(parameters);
    },
    warLogin: function(e) {
      e.preventDefault();
      var view = this;
      $.ajax({
        url: 'https://api.worldoftanks.ru/wot/auth/login/?' + $.param({
          application_id: constants.wotAppId,
          nofollow: 1,
          redirect_uri: window.location.host
        }),
        success: function(data) {
        console.log('data', data);
            if (data.status == 'ok') {
              var loginWindow = window.open(data.data.location, "Wargamin login", "width=700, height=700, left=300, top=100");
              var timerChecking = setInterval(function() {
                try {
                  console.log('loginWindow.location.host', loginWindow.location.host);
                  console.log('window.location.host', window.location.host);
                  if (loginWindow.location.host == window.location.host) {
                    view.decodeWarParams(loginWindow.location.search)
                    loginWindow.close();
                    clearInterval(timerChecking);
                  }
                } catch (e) {
                  console.log(e);
                }
              }, 100)
            }
          }
          // url: 'https://api.worldoftanks.ru/wot/auth/login/?application_id=c46a776821d6eb69e6c2e3d962fc7eff&nofollow=1',
      })

    }

  });

  return AccView;


});
