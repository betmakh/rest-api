define([
  'marionette',
  'app',
  'account/templates/templates',
  'constants/constants',
  'entities/items'

], function(Marionette, app, templates, constants, productAPI) {

	var ProductEditView = Marionette.ItemView.extend({
		className: 'container',
		template: templates.account,
    events: {
      'focus input': 'inputStart',
      'focus textarea': 'inputStart',
      'blur textarea': 'inputEnd',
      'blur input': 'inputEnd'
    },
    savedSuccess: function(field) {
      field.classList.add('success');
      setTimeout(function() {
        field.classList.remove('success');
      }, 2000);
    },
    savedFail: function(field) {
      field.classList.add('error');
    },
    inputEnd: function(e) {
      var label = e.target.getAttribute('name');
        value = e.target.value,
        view = this;
      this.model.save(label, value).then(function() {
        view.savedSuccess(e.target);
      }, function() {
        view.savedFail(e.target);
      });
    },
    inputStart: function(e) {
      $(e.target).data('old', e.target.value);
    },
    onRender: function() {
      this.$('#proceed').remove();
      this.$('.loginWot').remove();
    }
    
});


	return ProductEditView;

})