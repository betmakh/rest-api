define([
  'marionette',
  'app',
  'product/templates/templates',
  'constants/constants',
  'account/tanksView',
  'constants/util',
  'entities/tanks',
  'login/api/fb',
  'login/api/vk',
  'login/api/ok'

], function(Marionette, app, templates, constants, TanksView, util) {

	var ProductView = Marionette.ItemView.extend({
		className: 'container',
		template: templates.product,
    events: {
      'click button.buy': 'buyAcc',
      'click .like': 'likeIt'
    },
    likeIt: function(e) {
      $(e.currentTarget).toggleClass('active');
      if (sessionStorage.google_id_token) {
        var request = this.model.like(sessionStorage.google_id_token);
      }
    },
    redirectForPayment: function(form) {
      var div = document.createElement('div');
      div.innerHTML = form;
      div.querySelector('form').submit();
      // document.location.href = url;
    },
    buyAcc: function(e) {
      var view = this;
      var headers = util.getAuthHeaders();
      if (headers) {
        $.ajax({
          url: '/api/buy/' + view.model.id,
          type: 'POST',
          headers: util.getAuthHeaders(),
          data: {
            price: view.model.get('price'),
            seller_id: view.model.get('user').id,
          },
          success: function(data) {
            view.redirectForPayment(data.form);
          }
        })
      }
      
    },
    onBeforeRender: function() {
      this.model.set('share_link', window.location.href);
    },
    onRender: function() {
      this.fetchTankCollection(this.model.get('tanks'));
      VK.Widgets.Like("vk_like", {type: "mini"});
      setTimeout(function () {
        OK.CONNECT.insertShareWidget("ok_shareWidget",window.location.href,"{width:125,height:25,st:'straight',sz:12,ck:1}");
      }, 0)
    },
    fetchTankCollection: function(ids) {
      var view = this;
      var tanks = new Backbone.Collection();
      var tanksArr = ids;
      view.userTanks = tanksArr;
      // view.getStatistic();
      ids.forEach(function(tank) {
        var tankInfo = app.request('tank:id', {
          application_id: constants.wotAppId,
          tank_id: tank
        });
        $.when(tankInfo).done(function(data){
          tanks.add(data);
          if (tanks.length === tanksArr.length) {
            if (!view.tanksView) {
              view.tanksView = new TanksView({collection:tanks})
            } else {
              view.tanksView.collection = tanks;
            }
            var tanksRegion = new Marionette.Region({
              el: view.$('#tanksList')
            });
            tanksRegion.show(view.tanksView)
          }
        })
      });
	}
});


	return ProductView;

})