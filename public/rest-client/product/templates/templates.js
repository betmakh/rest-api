define([
    'underscore',
    'handlebars',
    'text!product/templates/product.hbs',
], function (_, Handlebars, product) {


    var templates = {
        product: Handlebars.compile(product),
    };

    return templates;
});