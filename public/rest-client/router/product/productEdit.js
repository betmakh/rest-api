define([
  'marionette',
  'app',
  'account/templates/templates',
  'router/defaultRouter',
  'layout/mainLayout',
  'product/productEditView'


], function(Marionette, app, templates, DefaultRouter, mainRegion, ProductEditView) {

  var ProductEditRoute = DefaultRouter.extend({
    prepare: function(params) {
      var id = params.params[0];
      this.acc = app.request('account:id', id);
      DefaultRouter.prototype.prepare.call(this);
      $(mainRegion.getInstance().features.el).addClass('box');
      // this.accs = app.request('accounts:list');
    },
    execute: function() {
      console.log(this.productId);
      $.when(this.acc).done(function(resp) {
        var view = new ProductEditView({
          model: resp
        });
        mainRegion.getInstance().features.show(view);
      });
    }
  });

  return ProductEditRoute;

})
