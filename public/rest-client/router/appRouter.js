define([
  'marionette',
  'app',
  'layout/headerLayout',
  'layout/mainLayout',
  'itemsView/gridView',
  'layout/footerView',
  'account/accountView',
  'login/login',
  'login/resetPasswordView',
  'layout/bannerView',
  'product/productView',
  'profile/profileView',
  'login/registrationView',
  'router/defaultRouter',
  'router/product/productEdit',
  'router/feedbacks/feedbacksRouter'
], function(
  Marionette,
  app,
  headerLayout,
  mainRegion,
  GridView,
  footerView,
  AccountView,
  LoginView,
  ResetPasswordView,
  BannerView,
  ProductView,
  ProfileView,
  RegistrationView,
  DefaultRouter,
  ProductEditRoute,
  FeedbacksRouter
) {

  console.log(BannerView);

  // mainRegion.render()
  var HomeRoute = DefaultRouter.extend({
    prepare: function() {
      DefaultRouter.prototype.prepare.call(this);
      mainRegion.getInstance().banner.show(new BannerView());
      $(mainRegion.getInstance().features.el).removeClass('box');
      this.accs = app.request('accounts:list');
    },
    execute: function() {
        console.log('before request done ');

      $.when(this.accs).done(function(resp) {
        var view = new GridView({
          collection: resp
        });
        mainRegion.getInstance().features.show(view);
      });
      mainRegion.getInstance().header.show(headerLayout);
      mainRegion.getInstance().footer.show(footerView);
    }
  });

  var LoginRoute = DefaultRouter.extend({
    execute: function() {
      mainRegion.getInstance().features.show(new LoginView())
    }
  });

  var RegistrationRoute = DefaultRouter.extend({
    execute: function() {
      mainRegion.getInstance().features.show(new RegistrationView())
    }
  })

  var SellRoute = DefaultRouter.extend({
    execute: function() {
      $(mainRegion.getInstance().features.el).addClass('box');
      mainRegion.getInstance().features.show(new AccountView({
        model: new Backbone.Model({
          title: 'nigga'
        })
      }))
    }
  })

  var ProductRoute = DefaultRouter.extend({
    prepare: function(params) {
      var id = params.params[0];
      this.acc = app.request('account:id', id);
      DefaultRouter.prototype.prepare.call(this);
      $(mainRegion.getInstance().features.el).addClass('box');
      // this.accs = app.request('accounts:list');
    },
    execute: function() {
      console.log(this.productId);
      $.when(this.acc).done(function(resp) {
        var view = new ProductView({
          model: resp
        });
        mainRegion.getInstance().features.show(view);
        DefaultRouter.prototype.execute.call(this);
      });
    }
  });



  var UserRoute = DefaultRouter.extend({
    prepare: function(params) {
      try {
        var user = JSON.parse(sessionStorage.getItem('user'));
      } catch (e) {
        app.navigate('/login');
        return
      }

      var id = params.params[0];
      if (id == 'me') {
        if (user) {
          this.acc = app.request('currentUser', user);
        }
      } else {
        this.acc = app.request('user:id', id);
      }
      DefaultRouter.prototype.prepare.call(this);
      $(mainRegion.getInstance().features.el).addClass('box');
      // this.accs = app.request('accounts:list');
    },
    execute: function(params) {
      console.log(this.productId);
      var id = params.params[0];
      if (this.acc) {
        $.when(this.acc).done(function(resp) {
          var view = new ProfileView({
            model: resp,
            isMyself: id == 'me' ? true : false
          });
          mainRegion.getInstance().features.show(view);
        });

      } else {
        document.location.hash = '#login';
      }
    }
  });


  var ForgotPasswordRoute = DefaultRouter.extend({
    execute: function() {
      mainRegion.features.show(new ResetPasswordView())
    }
  });

  var AppRouter = Backbone.Blazer.Router.extend({
    routes: {
      '': new HomeRoute(),
      'sell': new SellRoute(),
      'login': new LoginRoute(),
      'register': new RegistrationRoute(),
      'feedbacks': new FeedbacksRouter(),
      'product/:id': new ProductRoute(),
      'profile/:id': new UserRoute(),
      'product/edit/:id': new ProductEditRoute(),
      'forgotPassword': new ForgotPasswordRoute()
    },
    filters: [{
      afterRoute: function() {
        // I run after execute() is called on all routes
        app.trigger('route', Backbone.history.getFragment());
      }
    }]
  })

  var router = new AppRouter();  

  return router;

});
