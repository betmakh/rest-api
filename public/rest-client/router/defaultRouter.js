define([
  'marionette',
  'app',
  'constants/constants',
  'layout/mainLayout',
  'layout/headerLayout',
  'layout/footerView',

], function(Marionette, app, constants, mainRegion, headerLayout, footerView) {


  var DefaultRouter = Backbone.Blazer.Route.extend({
    prepare: function() {
      // app.main.empty();
      // 
      console.log('default router');
      app.main.show(mainRegion.getInstance());
      mainRegion.getInstance().features.reset();
      // mainRegion.getInstance().reset();

      if (!mainRegion.getInstance().header.hasView()) {
        mainRegion.getInstance().header.show(headerLayout)
      }
      if (mainRegion.getInstance().banner.hasView()) {
        mainRegion.getInstance().banner.reset();
      }
      if (!mainRegion.getInstance().footer.hasView()) {
        mainRegion.getInstance().footer.show(footerView);
      }


    },
    execute: function() {
      console.log('scrolltop');
      $(document).scrollTop(0);
    }
  });

  return DefaultRouter;

})
