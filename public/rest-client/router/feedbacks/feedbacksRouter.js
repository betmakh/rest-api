define([
  'marionette',
  'app',
  'account/templates/templates',
  'router/defaultRouter',
  'layout/mainLayout',
  'feedbacks/feedbackCollectionView',
  'constants/util'

], function(Marionette, app, templates, DefaultRouter, mainRegion, FeedbackCollectionView,util) {

  var FeedbacksRouter = DefaultRouter.extend({
    prepare: function(params) {
        console.log('params', params);

      this.feedbacks = app.request('feedbacks:list', util.parseQueryString(params.params.join('&')));
      DefaultRouter.prototype.prepare.call(this);
      $(mainRegion.getInstance().features.el).addClass('box');
    },
    execute: function() {
        var self = this;

        $.when(this.feedbacks).done(function(resp) {
          var view = new FeedbackCollectionView({
            collection: resp,
            user: self.user
          });
          mainRegion.getInstance().features.show(view);
      
        });
    }
  });

  return FeedbacksRouter;

})
