define([
  'handlebars',
  'marionette',
  'app',
  'login/templates/templates',
  'constants/constants',
  'constants/util',
  'md5',
  'validator'
], function(Handlebars, Marionette, app, templates, constants, util, md5) {

  var loginCallback = function(data) {
    var user = {
      id_token: data.token,
      name: data.name,
      avatar: data.avatar
    }
    sessionStorage.setItem('user', JSON.stringify(user));
    Backbone.trigger('login', user);
  }

  var loginView = Marionette.ItemView.extend({
    template: templates.login,
    className: 'container',
    events: {
      'click #googleLogout': 'logout',
      'click .google': 'loginGoogle',
      'click .odnoklassniki': 'loginOK',
      'click .vk': 'loginVK',
      'click .submit': 'login'
    },
    onRender: function() {
      this.form = this.el.querySelector('form');
      this.validator = $(this.form).validate();
    },
    login: function(e) {
      e.preventDefault();
      var form = this.form;
      console.log('this.validator', this.validator);
      this.validator.form();
      // util.validateForm(form);
      if (!form.querySelector('input.error')) {
        var data = util.form2json(form);
        $.ajax({
          url: 'api/auth/user',
          type: 'POST',
          data: data,
          success: loginCallback
        });
      }
    },
    openAuthWindow: function(opts, cb) {
      var loginWindow = open(opts.url, opts.name, "width=700, height=700, left=300, top=100");
      var timerChecking = setInterval(function() {
        try {
          if (loginWindow.location.host === window.location.host) {
            loginWindow.onunload = function() {
              cb(util.parseQueryString(loginWindow.location.search).code);
            }
            loginWindow.close();
            clearInterval(timerChecking);
          }
        } catch (e) {
          console.log(e);
        }
      }, 100)
    },
    loginVK: function(e) {
      var view = this,
        url = '//oauth.vk.com/authorize?' + $.param({
          client_id: constants.vk.appId,
          scope: 'email',
          redirect_uri: window.location.origin + '/auth/ok',
          response_type: 'code'
        })

      var sendCode = function(code) {
          console.log('code', code);
        $.ajax({
          url: '/api/auth/vk',
          type: 'POST',
          data: {
            code: code
          },
          success: function(data, status, jqxhr) {
              console.log('data', data);
            var user = {
              id_token: data.token,
              name: data.name,
              type: 'vk',
              avatar: data.avatar
            }
            sessionStorage.setItem('user', JSON.stringify(user));
            Backbone.trigger('login', user);
          },
          error: function(data) {
              console.log(data);

            }
            // dataType: 
        })
      }
      this.openAuthWindow({ url: url, name: 'VK auth' }, sendCode)
    },
    loginOK: function() {

      var view = this,
        url = '//connect.ok.ru/oauth/authorize?' + $.param({
          client_id: constants.odnoklassniki.appId,
          scope: 'GET_EMAIL',
          response_type: 'code',
          redirect_uri: window.location.origin + '/auth/ok',
          layout: 'a'
        });
      console.log(window.location.origin + '/auth/ok');

      var sendCode = function(code) {
        $.ajax({
          url: '/api/auth/ok',
          type: 'POST',
          data: {
            code: code
          },
          success: function(data, status, jqxhr) {
            console.log(data);
            var user = {
              id_token: data.token,
              name: data.name,
              type: 'ok',
              avatar: data.avatar
            }
            sessionStorage.setItem('user', JSON.stringify(user));
            Backbone.trigger('login', user);
          },
          error: function(data) {
              console.log(data);

            }
            // dataType: 
        })
      }

      this.openAuthWindow({
        url: url,
        name: '"Odnoklassniki login"'
      }, sendCode)
    },
    loginGoogle: function() {
      var view = this;
      // auth2.signIn(function(data){
      //   console.log(data);
      // })
      auth2.grantOfflineAccess({
        'redirect_uri': 'postmessage'
      }).then(function(authResult) {
        console.log(authResult);
        var auth = gapi.auth2.getAuthInstance();
        var gUser = auth.currentUser.get();
        // sessionStorage.setItem('google_id_token', gUser.getAuthResponse().id_token);

        //if new 201
        //api user update
        //202 old
        $.ajax({
          url: '/api/auth/google',
          type: 'POST',
          data: {
            code: authResult.code
          },
          success: function(data, status, jqxhr) {
            var user = {
              id_token: gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token,
              type: 'google',
              name: gUser.getBasicProfile().getName(),
              avatar: gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getImageUrl()
            }
            sessionStorage.setItem('user', JSON.stringify(user));
            Backbone.trigger('login', user);
          },
          error: function(data) {
              console.log(data);

            }
            // dataType: 
        })
      });
    },
    // onRender: function(){
    //   var auth = gapi.auth2.getAuthInstance();
    //   if (auth.isSignedIn.get()) {
    //     this.$('google').css('display','none');
    //   }
    // },
    logout: function(e) {
      e.preventDefault();
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function() {
        console.log('User signed out.');
      });

    },

  });

  return loginView;

});
