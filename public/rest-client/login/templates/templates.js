define([
    'underscore',
    'handlebars',
    'text!login/templates/login.hbs',
    'text!login/templates/register.hbs',
    'text!login/templates/resetPassword.hbs',
    'text!login/templates/resetPasswordConfirmation.hbs',
], function (_, Handlebars, login, register, resetPassword, resetPasswordConfirmation) {


    var templates = {
        login: Handlebars.compile(login),
        register: Handlebars.compile(register),
        resetPassword: Handlebars.compile(resetPassword),
        resetPasswordConfirmation: Handlebars.compile(resetPasswordConfirmation),
    };

    return templates;
});