define([
  'handlebars',
  'marionette',
  'app',
  'login/templates/templates',
  'constants/constants',
  'constants/util',
  'entities/profile',
  'underscore',
  'layout/LoaderView',
  'validator'
], function(Handlebars, Marionette, app, templates, constants, util, userAPI, _, LoaderView) {


  var loginView = Marionette.ItemView.extend({
    template: templates.register,
    events: {
      'change input[name="email"]': 'changeEmail',
      'click .submit': 'submitForm'
    },
    forbiddenEmails: [],
    onRender: function() {
      // validation rules
      var self = this;
      jQuery.validator.addMethod("emailExist", function(value, element) {
        return self.forbiddenEmails.indexOf(value.trim()) < 0;
      }, "That email is already in use.");
      this.validator = this.$('form').validate();
      this.$('[name="passwordConfirm"]').rules('add', {
        equalTo: '[name="password"]'
      });
      this.$('[name="email"]').rules('add', {
        emailExist: true
      });
    },
    submitForm: function(e) {

      e.preventDefault();
      this.validator.form();
      var form = this.el.querySelector('form'),
        data = util.form2json(this.el.querySelector('form'));

      if (!form.querySelector('input.error')) {
        setTimeout(function() {
          $('#features-wrapper').html((new LoaderView()).render().el);
        }, 1)
        userAPI.createUser(data).then(function(data) {
          var user = {
            id_token: data.token,
            name: data.name,
            type: 'default',
          }
          sessionStorage.setItem('user', JSON.stringify(user));
          Backbone.trigger('login', user);
        });
      }
    },
    changeEmail: function(e) {
      var val = e.target.value;
      var forbiddenEmails = this.forbiddenEmails;
      var self = this;

      if (val.length && forbiddenEmails.indexOf(val) < 0) {
        $.ajax({
          url: 'api/users/isEmailAvailable',
          data: { email: val },
          success: function(resp) {
            if (resp && !resp.isFree) {
              forbiddenEmails.push(val);
              self.forbiddenEmails = forbiddenEmails;
            }
          }
        })
      }
    },
    className: 'container box',



  });

  return loginView;

});
