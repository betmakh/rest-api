define([], function() {
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s);
    js.id = id;
    js.setAttribute('acync', 'acync')
    js.setAttribute('defer', 'defer')
    js.src = "//apis.google.com/js/platform.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'google-jssdk'));
})
