define(['constants/constants', 'facebook'], function(constants){
  FB.init({
    appId      : constants.fbAppId,
    version    : 'v2.4'
  });
  FB.getLoginStatus(function(response) {
    console.log(response);
  });
});