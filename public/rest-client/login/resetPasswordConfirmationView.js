define([
  'handlebars',
  'marionette',
  'app',
  'login/templates/templates',
  'constants/constants',
  'constants/util',
  'entities/profile',
  'underscore',
  'layout/LoaderView',
  'validator'
], function(Handlebars, Marionette, app, templates, constants, util, userAPI, _, LoaderView) {


  var resetConfirmation = Marionette.ItemView.extend({
    template: templates.resetPasswordConfirmation,
    events: {
      'click .submit': 'submitForm'
    },
    onRender: function() {
      // validation rules
      var self = this;
      this.validator = this.$('form').validate();
      this.$('[name="passwordConfirm"]').rules('add', {
        equalTo: '[name="password"]'
      });
    },
    submitForm: function(e) {

      e.preventDefault();
      this.validator.form();
      var form = this.el.querySelector('form'),
        data = util.form2json(this.el.querySelector('form'));

      if (!form.querySelector('input.error')) {
        userAPI.resetPasswordConfirm(data).then(resp => {
          util.getAuthHeaders();
          // sessionStorage.setItem('user', JSON.stringify(user));
          // Backbone.trigger('login', user);
        });
      }
    },
    className: 'container box',
  });

  return resetConfirmation;

});