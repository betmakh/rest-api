define([
    'marionette',
    'app',
    'profile/templates/templates',
    'constants/constants',
    'profile/userSellsView',
    'profile/ordersView',
    'entities/profile',
    'entities/orders'

], function(Marionette, app, templates, constants, UserSellsView, OrdersView) {

    var ProfileView = Marionette.ItemView.extend({
        className: 'container',
        template: templates.profile,
        events: {
            'dblclick .editable': 'editField',
            'blur .edit input': 'endEdit'
        },
        endEdit: function(e) {
            var val = e.target.value,
                oldVal = $(e.target).data('old');

            if (val != oldVal) {

            }
        },
        editField: function(e) {
            e.target.classList.add('edit');
            $(e.target).data('old', e.target.value);
        },
        initialize: function(options) {
            _.extend(this, options);
        },
        onRender: function() {
            var userId = this.model.id,
                view = this,
                regions = {
                    accsRegion: new Marionette.Region({
                        el: view.$('#userAccs')
                    }),
                    buyerOrders: new Marionette.Region({
                        el: view.$('#buys')
                    }),
                    sellerOrders: new Marionette.Region({
                        el: view.$('#sells')
                    })
                }


            var AccsCollection = app.request('accounts:collectionClass');
            var accs = new AccsCollection(this.model.get('accounts'));
            var accsView = new UserSellsView({
                collection: accs
            });

            regions.accsRegion.show(accsView);


            if (!this.isMyself) {
                this.$('.editColumn').remove();
                this.$('.deleteColumn').remove();
                this.$('.delete').closest('td').remove();
                this.$('.edit').closest('td').remove();

            } else {
                var OrdersCollection = app.request('order:collectionClass');
                var ordersSell = new OrdersCollection(this.model.get('ordersSell'));
                var sellsView = new OrdersView({
                    collection: ordersSell
                });
                regions.sellerOrders.show(sellsView);

                var ordersBuy = new OrdersCollection(this.model.get('ordersBuy'));
                var buysView = new OrdersView({
                    collection: ordersBuy
                });
                regions.buyerOrders.show(buysView);
            }

        }
    });

    return ProfileView;

})
