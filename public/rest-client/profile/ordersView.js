define([
  'handlebars',
  'marionette',
  'app',
  'profile/templates/templates',
  'constants/constants',
  'profile/userSellsView',
  'entities/profile'

], function(Handlebars,Marionette, app, templates, constants, UserSellsView) {

	var OrderView = Marionette.ItemView.extend({
    template: templates.order,
    tagName: 'tr',
    onRender: function() {
      console.log(this.model);
    }

  })

  var OrdersView = Marionette.CompositeView.extend({
		// className: 'container',
    childView: OrderView,
    childViewContainer: 'tbody',
		template: templates.orders,
    initialize: function(options) {
      _.extend(this,options);
    },
    onBeforeRender: function() {
      var view = this;
      console.log(this.collection);
      this.template = Handlebars.compile(this.template({type: view.type}));
    }
	});


	return OrdersView;

})