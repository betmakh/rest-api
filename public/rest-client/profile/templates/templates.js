define([
    'underscore',
    'handlebars',
    'text!profile/templates/profile.hbs',
    'text!profile/templates/accs.hbs',
    'text!profile/templates/acc.hbs',
    'text!profile/templates/orders.hbs',
    'text!profile/templates/order.hbs',
], function (_, Handlebars, profile, accs, acc, orders, order) {

    var templates = {
        profile: Handlebars.compile(profile),
        accs: Handlebars.compile(accs),
        acc: Handlebars.compile(acc),
        orders: Handlebars.compile(orders),
        order: Handlebars.compile(order),
    };

    return templates;
});