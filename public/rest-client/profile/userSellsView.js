define([
  'marionette',
  'app',
  'profile/templates/templates',
  'constants/constants',
  'entities/items'

], function(Marionette, app, templates, constants) {

	var AccView = Marionette.ItemView.extend({
			events: {
				'click .delete': 'removeAcc'
			},
			removeAcc: function(e) {
				var isConfirmed = confirm('Are you sure you want to delete account?');
				if (isConfirmed) {
					var request = this.model.destroy({
						success: function() {
							console.log('destroyed');
						}
					});
				}
			},
			template: templates.acc,
			tagName: 'tr'
	});

	var AccsView = Marionette.CompositeView.extend({
		// className: 'container',
		childView: AccView,
		childViewContainer: 'tbody',
		template: templates.accs,
		initialize: function(options) {
			_.extend(this,options);
		},
		// onRender: function() {
		// 	// if (!this.isMyself) {
  //  //      this.$('.delete').hide();
  //  //    }
		// }
	});

	return AccsView;

})