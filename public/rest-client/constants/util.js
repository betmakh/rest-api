define(['jquery'], function ( $) {
  return {
    getAuthHeaders: function () {
      var notAuthorized = function () {
        require(['router/appRouter'], function (router) {
          router.navigate('/login', {trigger: true});
        })
      };
      var user = this.getUser();
      
      if (user && user.id_token) {
        return {
          authorization: user.id_token,
          authorizationType: user.type
        }
      } else {
        notAuthorized();
      }
    },
    getUser: function() {
      try {
        var user = JSON.parse(sessionStorage.getItem('user'));
      } catch (e) {
        return null;
      }
      if (user && user.id_token) {
        return user
      } else {
        return null;
      }
    },
    form2json: function (form) {
      var result = {};
      $(form).find('[name]').each(function(index, el) {
        result[el.getAttribute('name')] = el.value;
      });

      return result;
    },
    parseQueryString: function (url = window.location.search) {
      if (url.indexOf('#') == 0 || url.indexOf('?') == 0) {
        url = url.substr(1);
      }
      var params = url.split('&');
      var parameters = {};
      params.forEach(function(el) {
        if (el.length >= 3) {
          var tmp = el.split('=');
          parameters[tmp[0]] = tmp[1];
        }
      });
      return parameters;
    }
  }
})