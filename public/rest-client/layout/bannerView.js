define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'layout/templates/templates'
], function(_, Handlebars, $, Marionette, app, templates) {

  var BannerView = Marionette.ItemView.extend({
    template: templates.banner,

  });

  return BannerView;

});
