define([
    'underscore',
    'handlebars',
    'text!layout/templates/header.hbs',
    'text!layout/templates/main.hbs',
    'text!layout/templates/footer.hbs',
    'text!layout/templates/banner.hbs',
], function (_, Handlebars, header, main, footer, banner) {


    var templates = {
        header: Handlebars.compile(header),
        main: Handlebars.compile(main),
        footer: Handlebars.compile(footer),
        banner: Handlebars.compile(banner),
    };

    return templates;
});