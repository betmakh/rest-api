define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'layout/templates/templates'
], function(_, Handlebars, $, Marionette, app, templates) {

  var LoaderRegion = Marionette.Region.extend({
    initialize: function() {
      this.$el.html('<div class="loader"></div>')
      console.log('initialize loader');
    },
    onEmpty: function() {
      this.$el.html('<div class="loader"></div>')
    }
  })

  var BasicLayout = Marionette.LayoutView.extend({
    template: templates.main,

    regions: {
      header: '#header-wrapper',
      features: {
        regionClass: LoaderRegion,
        el: "#features-wrapper" 
      },
      main: {
        el: "#main-wrapper"
      },
      footer: '#footer-wrapper',
      banner: '#banner-wrapper'
    }
  });

  return {
    getInstance: function() {
      if (!this.instance) {
        this.instance = new BasicLayout();
      }
      return this.instance;
    }
  };

});
