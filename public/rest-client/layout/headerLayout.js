define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'layout/templates/templates',
  'login/login'
], function(_, Handlebars, $, Marionette, app, templates, LoginView) {

  var HeaderLayout = Marionette.ItemView.extend({
    template: templates.header,
    events: {
      // 'click #nav li': 'selectCurent',
      'click .logout': 'logout',
      'click .loginPopup .odnoklassniki': 'okLogin',
      'click .loginPopup .google': 'googleLogin',
      'click .loginPopup .vk': 'vkLogin',
    },
    okLogin: function(e) {
      LoginView.prototype.loginOK()
    },
    googleLogin: function(e) {
      LoginView.prototype.loginGoogle()
    },
    vkLogin: function(e) {
      LoginView.prototype.loginVK()
    },
    logout: function(e) {
      e.preventDefault();
      var auth2 = gapi.auth2.getAuthInstance();
      Backbone.trigger('logout');

      auth2.signOut();
    },
    initialize: function() {
      var view = this;
      view.listenTo(app, 'route', function(route) {
        view.selectCurent(route);
      })
      view.listenTo(Backbone, 'login', function(opts) {
        view.profile = opts;
        view.render();
      })
      view.listenTo(Backbone, 'logout', function() {
        view.profile = {};
        view.render();
      })
    },
    selectCurent: function(route) {
      this.$('#nav li').removeClass('current').each(function(index, el) {
        if (route.length && $(el).find('a').attr('href').indexOf(route) != -1) {
          console.log(el);
          el.classList.add('current')

        }
      });


      // $(e.currentTarget).addClass('current');
    },
    render: function() {
      console.log(this.profile);
      this.$el.html(this.template(this.profile));
    }



  });

  return (new HeaderLayout());

});
