define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'layout/templates/templates'
], function(_, Handlebars, $, Marionette, app, templates) {

  var LoaderView = Backbone.View.extend({
    render: function() {
      this.$el.html('<div class="loader"></div>');
      return this;
    }
  })

  return LoaderView;
});
