define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'layout/templates/templates'
], function(_, Handlebars, $, Marionette, app, templates) {

  var FooterView = Marionette.ItemView.extend({
    template: templates.footer,
  });

  return new FooterView();

});
