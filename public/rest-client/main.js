require.config({
  waitSeconds: 200,
  // baseUrl: 'rest-api',

  paths: {
    'facebook': '//connect.facebook.net/en_US/sdk',
    "underscore": "bower_components/underscore/underscore",
    "handlebars": "bower_components/handlebars/handlebars",
    "backbone": "bower_components/backbone/backbone",
    'backbone.blazer': "bower_components/backbone.blazer/backbone.blazer",
    // 'backbone.radio': "bower_components/backbone.radio/build/backbone.radio",
    "marionette": "bower_components/backbone.marionette/lib/backbone.marionette",
    // "tpl": 'templates/templates',
    "text": "bower_components/text/text",
    "jquery": "bower_components/jquery/dist/jquery",
    'google': '//apis.google.com/js/platform',
    'vkontakte': '//vk.com/js/api/openapi',
    'odnoklassniki': '//connect.ok.ru/connect',
    'validator': 'bower_components/jquery-validation/dist/jquery.validate',
    'md5': 'bower_components/js-md5/js/md5'
  },
  shim: {
    "underscore": {
      exports: "_"
    },
    "validator": {
      deps: ['jquery']
    },
    'odnoklassniki': {
      exports: 'OK'
    },
    'facebook': {
      exports: 'FB'
    },
    'vkontakte': {
      exports: 'VK'
    },
    backbone: {
      deps: ["underscore", "jquery"],
      exports: "Backbone"
    },
    'backbone.blazer': {
      deps: ['backbone'],
    },
    marionette: {
      deps: ["backbone", "backbone.blazer", 'underscore', 'handlebars'],
      exports: "Marionette"
    },
    "handlebars": {
      exports: "Handlebars"
    }
  }
});

require([
  'app',
  'router/appRouter',
  'validator',
  'entities/items',
  'google'

], function(app, router) {

  console.log('anus')
  window.app = app;
  app.start();


  Backbone.on('logout', function() {
    sessionStorage.removeItem('user');
    router.navigate('/', { trigger: true });

  });

  Backbone.on('login', function(user) {
    if (!user.initial) {
      router.navigate('/profile/me', { trigger: true });
    }
  });

  $(document).on("click", "a[href]:not([data-bypass])", function(evt) {
    // Get the absolute anchor href.
    var href = { prop: $(this).prop("href"), attr: $(this).attr("href") };
    // Get the absolute root.
    var root = location.protocol + "//" + location.host;

    // Ensure the root is part of the anchor href, meaniЫng it's relative.
    if (href.prop.slice(0, root.length) === root) {
      // var link = 
      // alert('stop')
      // Stop the default event to ensure the link will not cause a page
      // refresh.
      evt.preventDefault();

      // Note by using Backbone.history.navigate, router events will not be
      // triggered.  If this is a problem, change this to navigate on your
      // router.
      router.navigate(href.attr, { trigger: true });
    }
  });



});
