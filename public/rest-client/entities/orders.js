define(["app", "google"], function(restApp) {

	var OrderModel = Backbone.Model.extend({
    idAttribute : '_id',
    urlRoot: '/api/orders/',
    // url: function() {
    //   return this.urlRoot + this.id;
    // }
	});

	var OrderCollection = Backbone.Collection.extend({
    url: '/api/orders',
    model: OrderModel
	});

  var API = {
    getOrdersCollectionClass: function() {
      return OrderCollection;
    },
    getBuyerOrders: function(id) {
      var collection = new OrderCollection();
      var defer = $.Deferred();
      collection.fetch({
        data: {
          buyer: id
        },
        success: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        },
        error: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        }
      })
      return defer.promise();
    },
    getSellerOrders: function(id) {
      var collection = new OrderCollection();
      var defer = $.Deferred();
      
      collection.fetch({
        data: {
          seller: id
        },
        success: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        },
        error: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        }
      });
      return defer.promise();

    },
    getOrder: function(id){
      var user = new ProfileModel({_id: id});
      return user.fetch();
    }
   
  }

  restApp.reqres.setHandler("order:id", function(id) {
    return API.getOrder(id);
  });

  restApp.reqres.setHandler("order:collectionClass", function() {
    return API.getOrdersCollectionClass();
  });
  restApp.reqres.setHandler("buyerOrders:id", function(id) {
    return API.getBuyerOrders(id);
  });
  restApp.reqres.setHandler("sellerOrders:id", function(id) {
    return API.getSellerOrders(id);
  });

  return;
});
