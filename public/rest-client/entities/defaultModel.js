define(["app", 'constants/util'], function(restApp, util) {

  var Model = Backbone.Model.extend({
    destroy: function (opts) {
      opts = opts || {};
      var authHeaders = util.getAuthHeaders();
      return Backbone.Model.prototype.destroy.call(this, _.extend(opts, {
        headers: authHeaders
      }));
    },
    save: function(key, val, options) {
      // Handle both `"key", value` and `{key: value}` -style arguments.
      var authHeaders = util.getAuthHeaders();

      if (!arguments.length) {
        return Backbone.Model.prototype.save.call(this, null, {
          headers: authHeaders
        });
      } else {
        if (typeof key === 'object') {
          return Backbone.Model.prototype.save.call(this, key, _.extend(val || {}, {
            headers: authHeaders
          }));
        } else {
          return Backbone.Model.prototype.save.call(this, key, val, {
            headers: authHeaders
          });
        }
      }
    },

  })

  return Model;
})
