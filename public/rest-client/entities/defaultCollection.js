define(["app", 'constants/constants'], function(restApp, constants) {

  var Collection = Backbone.Collection.extend({
    parse: function(resp) {
      if (resp.data) {
        this.options = resp.options;
        return resp.data;
      } else {
        return [];
      }
    }
  })

  return Collection;
})
