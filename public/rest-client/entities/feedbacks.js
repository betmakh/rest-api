define(["app", 'entities/defaultModel', 'entities/defaultCollection' ,"google"], function(restApp,DefaultModel, DefaultCollection) {

	var FeedbackModel = DefaultModel.extend({
    idAttribute : '_id',
    urlRoot: '/api/feedbacks/',
    // url: function() {
    //   return this.urlRoot + this.id;
    // }
	});

	var FeedbacksCollection = DefaultCollection.extend({
    url: '/api/feedbacks',
    model: FeedbackModel
	});

  var API = {
    getFeedbacks: function(opts) {
      var feedbacks = new FeedbacksCollection();
      var defer = $.Deferred();
      opts = opts || {
        page: 0,
        limit: 10
      };
      // opts = _.extend(opts, {
      //   page: 0,
      //   limit: 10
      // });
      console.log('opts', opts);
      
      feedbacks.fetch({
        data: opts,
        success: function(collection, xhr, options) {
          defer.resolve(collection, xhr, options);
        },
        error: function(collection, xhr, options) {
          defer.resolve(collection, xhr, options);
        }
      })
      return defer.promise();
    },
    getFeedbackModel: function() {
      return new FeedbackModel();
    }
  }

  restApp.reqres.setHandler("feedbacks:list", function(opts) {
    return API.getFeedbacks(opts);
  });

  restApp.reqres.setHandler("feedback:model", function() {
    return API.getFeedbackModel();
  });


  return;
});
