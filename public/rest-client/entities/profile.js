define(["app", "google"], function(restApp) {

  var ProfileModel = Backbone.Model.extend({
    idAttribute: '_id',
    urlRoot: '/api/users/',
    // url: function() {
    //   return this.urlRoot + this.id;
    // }
  });

  // var AccountsCollection = Backbone.Collection.extend({
  //    url: '/api/accounts'
  // });

  var API = {
    getUser: function(id) {
      var user = new ProfileModel({ _id: id });
      var options = options || {};
      var defer = $.Deferred();
      user.fetch({
        success: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        },
        error: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        }
      })
      return defer.promise();
    },
    resetPassword: function(email) {
      return $.ajax({
        url: 'api/users/resetPassword',
        data: { email: email },
        type: 'POST'
      });
    },
    resetPasswordConfirm: function(data) {
      return $.ajax({
        url: 'api/users/resetPasswordConfirm',
        data: data,
        type: 'POST'
      });
    },
    createUser: function(data) {
      var model = new ProfileModel(data);
      return model.save()
    },
    getCurrentUser: function(user) {
      var defer = $.Deferred();
      $.ajax({
          url: '/api/users/0',
          headers: {
            authorization: user.id_token,
            authorizationType: user.type
          }
        })
        .done(function(data, xhr, options) {
          var model = new ProfileModel(data);
          defer.resolve(model, xhr, options);
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

      return defer.promise();

    }
  }

  restApp.reqres.setHandler("user:id", function(id) {
    return API.getUser(id);
  });
  restApp.reqres.setHandler("currentUser", function(id_token) {
    return API.getCurrentUser(id_token);
  });

  return API;
});
