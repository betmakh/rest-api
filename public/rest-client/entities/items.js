define(["app",'entities/defaultModel'], function(restApp, DefaultModel) {

	var AccountModel = DefaultModel.extend({
    idAttribute : '_id',
    urlRoot: '/api/accounts/',
    like: function(token) {
      var model = this;
      return $.ajax({
        url: model.url(),
        type: 'PATCH',
        headers: {
          authorization: token,
          type: 'like'
        },
        success: function(data) {
          model.fetch();
        }
      });
      
    }
    // url: function() {
    //   return this.urlRoot + this.id;
    // }
	});

	var AccountsCollection = Backbone.Collection.extend({
    url: '/api/accounts',
    model: AccountModel
	});

  var API = {
    getAccountsCollectionClass: function() {
      return AccountsCollection;
    },
    getAccounts: function(options) {
      var collection = new AccountsCollection();
      // var options = options || {};
      var defer = $.Deferred();
      var data = {};
      if (options) {
        data.user = options.user;
      }
      collection.fetch({
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", sessionStorage.google_id_token);
        },
        data: data,
        success: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        },
        error: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        }
      });
      return defer.promise();
      // var testCollection = new AccountsCollection([{title: 'Account 1', id: 1, price: 220}, {title:'Igor',id: 2},{title:'Igor22', id: 3}]);
      // console.log(testCollection);
      // return testCollection;
    },
    getModel: function() {
      var model = new AccountModel({});
      return model;
    },
    getAccount: function(id) {
      var model = new AccountModel({_id: id});
      var defer = $.Deferred();
      console.log(model);
      model.fetch({
       success: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        },
        error: function(model, xhr, options) {
          defer.resolve(model, xhr, options);
        } 
      });
      return defer.promise();

    }
  }

  restApp.reqres.setHandler("accounts:list", function(options) {
    return API.getAccounts(options);
  });
  restApp.reqres.setHandler("account:model", function() {
    return API.getModel();
  });
  restApp.reqres.setHandler("accounts:collectionClass", function() {
    return API.getAccountsCollectionClass();
  });
  restApp.reqres.setHandler("account:id", function(id) {
    return API.getAccount(id);
  });
  

  return API;
});
