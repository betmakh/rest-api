define(["app"], function(restApp) {


  var API = {
    getUserTanks: function(options) {
      var defer = $.Deferred();
      $.ajax({
          url: '//api.worldoftanks.ru/wot/account/tanks/?' + $.param({
            application_id: options.application_id,
            account_id: options.account_id
          })
        })
        .done(function(data) {
          var collection = new Backbone.Collection(data.data[options.account_id]);
          defer.resolve(collection);
        })
        .fail(function() {
          console.log("error");
        })
      return defer.promise();

    },
    getUserData: function(options) {
      var defer = $.Deferred();

      $.ajax({
          url: '//api.worldoftanks.ru/wot/account/info/?' + $.param({
            application_id: options.application_id,
            account_id: options.account_id,
            access_token: options.access_token,
            fields: options.fields || 'private.gold, account_id, ban_info, nickname, statistics.all.wins, statistics.all.battles, private.is_bound_to_phone, private.free_xp'
          })
        })
        .done(function(data) {
          defer.resolve(data.data[options.account_id]);
        })
        .fail(function() {
          console.log("error");
        })
      return defer.promise();

    },
    getTanks: function(options) {
      var defer = $.Deferred();
      $.ajax({
          url: '//api.worldoftanks.ru/wot/encyclopedia/tanks/?' + $.param({
            application_id: options.application_id,
          })
        })
        .done(function(data) {
          var collection = new Backbone.Collection(data.data);
          defer.resolve(collection);
        })
        .fail(function() {
          console.log("error");
        })
      return defer.promise();
    },
    getTank: function(options) {
      var defer = $.Deferred();
      $.ajax({
          url: '//api.worldoftanks.ru/wot/encyclopedia/tankinfo/?' + $.param({
            application_id: options.application_id,
            tank_id: options.tank_id
          })
        })
        .done(function(data) {
          var obj = data.data[options.tank_id];
          obj.image = obj.image.substring(obj.image.indexOf('://') + 1,obj.image.length)
          var tank = new Backbone.Model(obj);
          defer.resolve(tank);
        })
        .fail(function(resp) {
          defer.reject(resp);
          console.log("error");
        })
      return defer.promise();
    }

  }

  restApp.reqres.setHandler("tanks:list", function(options) {
    return API.getTanks(options);
  });
  restApp.reqres.setHandler("userTanks:list", function(options) {
    return API.getUserTanks(options);
  });
  restApp.reqres.setHandler("userData", function(options) {
    return API.getUserData(options);
  });
  restApp.reqres.setHandler("tank:id", function(options) {
    return API.getTank(options);
  });


  return;
});
