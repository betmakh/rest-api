define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'itemsView/templates/templates',
  'itemsView/gridItemView',
  'entities/items'
], function(_, Handlebars, $, Marionette, app, templates, GridItemView) {

  var GridView = Marionette.CompositeView.extend({
    template: templates.grid,
    // itemView: GridItemView,
    childView: GridItemView,
    className: 'container',
    childViewContainer: '#features-row',

  });


  return GridView;

});
