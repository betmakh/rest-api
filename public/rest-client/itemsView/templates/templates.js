define([
    'underscore',
    'handlebars',
    'text!itemsView/templates/grid.hbs',
    'text!itemsView/templates/grid-item.hbs',
], function (_, Handlebars, grid, gridItem) {


    var templates = {
        grid: Handlebars.compile(grid),
        gridItem: Handlebars.compile(gridItem),
    };

    Handlebars.registerHelper('randomImg', function(options) {
    	var num = Math.floor(Math.random()*3);
		  return ++num;
		});
    return templates;
});