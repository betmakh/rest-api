define([
  'marionette',
  'constants/constants'
], function(Marionette, constants) {

  var RestApp = Marionette.Application.extend({
    onStart: function() {
      // Backbone.history.start();
      Backbone.history.start({pushState: true})
      gapi.load('auth2', function() {
        window.auth2 = gapi.auth2.init({
          client_id: constants.googleAppId,
        });
        if (sessionStorage.user) {
          try {
            var user = JSON.parse(sessionStorage.user);
            Backbone.trigger('login', _.extend(user,{
              initial: true
            }));
          } catch(e) {
            // statements
            console.log(e);
          }
        }
        // console.log('auth2 loaded');

        // var auth = gapi.auth2.getAuthInstance();
        // auth.isSignedIn.listen(function(isSigned){
        //   console.log('user sign in');
        //   if (!isSigned) {
        //     sessionStorage.setItem('google_id_token', '')
        //     Backbone.trigger('logout');
        //   } else {
        //     Backbone.trigger('login',{type: 'google', name:auth.currentUser.get().getBasicProfile().getName()});
        //     sessionStorage.setItem('google_id_token', auth.currentUser.get().getAuthResponse().id_token)
        //   }
        // })

      });
    }
  });

  var app = new RestApp();

  app.addRegions({
    main: '.homepage',
  });

  

  return app;

});
