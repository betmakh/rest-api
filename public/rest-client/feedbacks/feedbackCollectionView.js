define([
  'underscore',
  'handlebars',
  'jquery',
  'marionette',
  'app',
  'feedbacks/templates/templates',
  'entities/feedbacks'
], function(_, Handlebars, $, Marionette, app, templates) {

  var FeedbackItemView = Marionette.ItemView.extend({
    template: templates.feedbackItem,
    className: 'row',
    onFetch: function(e) {
      console.log(this.model);
      this.render()
    },
    modelEvents: {
      'sync': 'onFetch'
    }
  })

  var FeedbacksCollectionView = Marionette.CompositeView.extend({
    className: 'container',
    events: {
      'click .send': 'createFeedback'
    },
    onRender: function () {
      var current = this.collection.options.page;
      console.log('this.options', this.collection.options);
      var pagionationData = [];
      while (pagionationData.length < this.collection.options.totalPages) {
        pagionationData.push({
          page: pagionationData.length,
          pageName: pagionationData.length + 1,
          isActive: pagionationData.length == current ? true : false
        })
      }
      this.$('[rel="pagination"]').html(templates.pagination(pagionationData));
      console.log('pagionationData', pagionationData);
    },
    createFeedback: function(e) {
      e.preventDefault();
      var model = app.request('feedback:model');
      var text = this.$('textarea').val();
      var rating = this.$('[name="rating"]:checked').val();

      model.set({
        text: text,
        rating: rating
      });
      this.collection.unshift(model)
      console.log(model);
      model.save({},{
        success: function() {
          console.log(arguments);
          model.fetch()
        }
      });
      console.log(model);
    },
    // initialize: function(){
    //   this.listenTo('Backbone', function(user) {

    //   })
    // },
    initialize: function(opts) {
      _.extend(this,opts);
      if (sessionStorage.user) {
        try {
          this.user = JSON.parse(sessionStorage.user);
        } catch(e) {
          console.warn(e);
          sessionStorage.removeItem('user');
        }
      }
      if (this.user) {
        var user = this.user;
        this.template = Handlebars.compile(this.template({user: user}));
      } else {
        this.template = Handlebars.compile(this.template({}));
      }
    },
    template: templates.feedbackCollection,
    childViewContainer: '.feedbacks-list',
    childView: FeedbackItemView
  })


  return FeedbacksCollectionView;

});
