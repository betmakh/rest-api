define([
  'underscore',
  'handlebars',
  'text!feedbacks/templates/feedback-item.hbs',
  'text!feedbacks/templates/feedback-collection.hbs',
  'text!feedbacks/templates/pagination.hbs',
], function(_, Handlebars, feedback, collection, pagination) {


  var templates = {
    feedbackItem: Handlebars.compile(feedback),
    feedbackCollection: Handlebars.compile(collection),
    pagination: Handlebars.compile(pagination),
  };

  Handlebars.registerHelper('lineSeparator', function(context, options) {
    var lines = context.split('\n');
    this.lines = lines;
    return options.fn(this);
  });

  return templates;
});
