var jwt = require('jsonwebtoken'),
  User = require('../db/models/userModel.js'),
  fs = require('fs');


module.exports = {
  getUser(data, cb) {
    if (data.email) {
      User.findOne({
        'email': data.email
      }, cb);
    } else {
      var id = data.id || data._id;
      User.findById(id, cb);
    }
  },
  detectUser: function(token, cb) {
    var self = this;
    this.decodeData(token).then(data => {
      self.getUser(data, cb)
    }, cb);
  },
  encodeUser: function(opts, cb) {
    var data = {},
      field = opts.fields || ['_id'],
      token = '';

    if (typeof opts.user === 'object') {
      field.forEach(el => {
        data[el] = opts.user[el];
      });
    } else {
      data['_id'] = opts.user;
    }

    Object.assign(data, opts.data || {});
    this.encodeData(data).then(token => {
      cb(null, token);
    }, cb);
  },
  encodeData(data) {
    return new Promise((resolve, reject) => {
      fs.readFile('key/id_rsa', (err, key) => {
        if (err) {
          console.log('err', err);
          reject(err);
        } else {
          var token = jwt.sign(data, key);
          resolve(token)
        }
      });
    });
  },
  decodeData(token) {
    return new Promise((resolve, reject) => {
      fs.readFile('key/id_rsa', (err, key) => {
        if (!err) {
          jwt.verify(token, key, (err, data) => {
            if (err) {
              reject(err);
            } else {
              resolve(data);
            }
          });
        } else {
          reject(err);
        }
      });
    })
  }
}
