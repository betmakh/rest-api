var express = require('express'),
  app = express(),
  _ = require('underscore'),
  request = require('request'),
  User = require('../db/models/userModel.js'),
  jwt = require('jsonwebtoken');



var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;

var CLIENT_ID = '976540656196-9l60unblu2cono0t9gcjk02sfu3ohor2.apps.googleusercontent.com',
  CLIENT_SECRET = '5XFgTIu688SxwuzWfeVM8tTf';
  REDIRECT_URL = 'https://nikita.accshop.org';

OAuth2.prototype.validateIdToken = function(token, cb) {
  console.log(token);

  var url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + token;

  request.post(
    url,
    null,
    function(error, response, body) {
      if (!error && response.statusCode == 200) {
        cb(null, body);
      } else {
        console.log('was posted ' + url);
        cb('not valid id token, try to relogin');
      }
    }
  );

};

OAuth2.prototype.detectUser = function(token, cb) {
  this.validateIdToken(token, function(err, resp) {
    if (err) {
      cb(err);
    } else {
      var decoded = jwt.decode(token);
      User.findOne({
        'email': decoded.email
      }, function(err, person) {
        if (err) {
          cb(err);
        } else {
        console.log(person);
          cb(null, person)
        }
      });
    }
  })

};

var oauth2Client = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

// generate a url that asks permissions for Google+ and Google Calendar scopes
var scopes = [
  'https://www.googleapis.com/auth/plus.me',
  'https://www.googleapis.com/auth/userinfo.email'
];


// var url = oauth2Client.generateAuthUrl({
//   access_type: 'offline', // 'online' (default) or 'offline' (gets refresh_token)
//   scope: scopes // If you only need one scope you can pass it as string
// });


module.exports = oauth2Client;
