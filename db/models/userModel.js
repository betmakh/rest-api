var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var relationship = require("mongoose-relationship");

var UserSchema   = new Schema({
    name: String,
    avatar: String,
    email: String,
    is_email_confirmed: {
      type: Boolean,
      default: false
    },
  	accounts: [{type: Schema.ObjectId, ref: 'Account'}],
  	feedbacks: [{type: Schema.ObjectId, ref: 'Feedback'}],
  	ordersBuy: [{type: Schema.ObjectId, ref: 'Order'}],
    ordersSell: [{type: Schema.ObjectId, ref: 'Order'}],
  	liked: [{type: Schema.ObjectId, ref: 'Account'}],
    rating: {
    	type: Number,
    	default: 0
    },
    password: String
});

module.exports = mongoose.model('User', UserSchema);