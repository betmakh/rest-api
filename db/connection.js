var mongoose   = require('mongoose');

mongoose.connect('mongodb://admin:admin@ds041563.mongolab.com:41563/accshop', {
  useMongoClient: true,
  /* other options */
}); // connect to our database

module.exports = mongoose;